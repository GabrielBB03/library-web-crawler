package capital.scalable;

import java.io.IOException;
import java.util.Comparator;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Collectors;

import capital.scalable.scrapers.GoogleScraper;
import capital.scalable.scrapers.ScriptScraper;

public class App {

    private static final short MAX_LIBRARIES = 5;

    public static void main(String[] args) throws IOException {
        try (Scanner in = new Scanner(System.in)) {
            System.out.println("Type your Google search term:");
            String term = in.nextLine();
            System.out.println("Getting Top " + MAX_LIBRARIES + " libraries for: " + term);
            System.out.println(getTopLibraries(term, new GoogleScraper(), new ScriptScraper()));
        }
    }

    /*
     * Easily mockable method to get the Top libraries used in google results pages
     */
    public static Set<String> getTopLibraries(String searchTerm, GoogleScraper googleScraper,
            ScriptScraper scriptScraper) throws IOException {
        /*
         * Make the Google Search, look for javascript libraries using multiple threads
         * , combine the returned libraries and group by name to get the frecuency.
         */
        Map<String, Long> countPerLibrary = googleScraper.getLinks(searchTerm).parallelStream()
                .map(scriptScraper::fromLink).flatMap(Set::stream)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        // Sort the map based on frecuencies and map that to a java.util.Set
        return countPerLibrary.entrySet().stream().sorted(Entry.comparingByValue(Comparator.reverseOrder()))
                .map(Entry::getKey).limit(MAX_LIBRARIES).collect(Collectors.toSet());
    }
}
