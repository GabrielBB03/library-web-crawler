package capital.scalable.scrapers;

/**
* A class to get javascript libraries used in a page HTML
* 
* @author Gabriel Basilio Brito
* 
*/

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.jsoup.Jsoup;

import capital.scalable.util.PathUtil;

public class ScriptScraper {

    public Set<String> fromLink(String link) {
        try {
            return fromHtml(Jsoup.connect(link).execute().body());
        } catch (IOException e) {
            System.err.println("Error scraping scripts for " + link + e);
            return new HashSet<>();
        }
    }

    public Set<String> fromHtml(String html) {
        return Jsoup.parse(html).select("script").stream().map(e -> PathUtil.getJsLibraryName(e.attr("src")))
                .filter(s -> s.endsWith(".js") && !s.equals("index.js")).collect(Collectors.toSet());
    }

}