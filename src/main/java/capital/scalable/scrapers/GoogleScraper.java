package capital.scalable.scrapers;

/**
* A class to get google results links based on a search term
* 
* @author Gabriel Basilio Brito
* 
*/

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class GoogleScraper {

    // This ResourceBundle gets the properties defined in pom.xml
    private static final ResourceBundle bundle = ResourceBundle.getBundle("app");

    private String getSearchLink(String term) throws UnsupportedEncodingException {
        return String.format(bundle.getString("google.results.url"), URLEncoder.encode(term, "UTF-8"), getMaxResults());
    }

    private int getMaxResults() {
        return Integer.parseInt(bundle.getString("google.results.max"));
    }

    public List<String> getLinks(String term) throws IOException {
        Document doc = Jsoup.connect(getSearchLink(term)).get();
        return doc.select(bundle.getString("google.results.selector")).stream().map(e -> e.attr("href"))
                .collect(Collectors.toList());
    }
}