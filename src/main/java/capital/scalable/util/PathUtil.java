package capital.scalable.util;

import java.io.File;

public class PathUtil {

    /**
     * This method handles every type of URL/path and returns just the javascript library name
     */
    public static String getJsLibraryName(String src) {
        if (src.contains("?")) {
            src = src.split("\\?")[0];
        }
        return new File(src).getName().toLowerCase().replace(".min.js", ".js").replace(".slim.js", ".js")
                .replace(".once.js", ".js");
    }
}