package capital.scalable;

import java.io.IOException;

import capital.scalable.scrapers.GoogleScraper;
import capital.scalable.util.PathUtil;
import junit.framework.TestCase;

public class AppTest extends TestCase {

    public AppTest(String testName) {
        super(testName);
    }

    public void testUrlDeduplication() {
        assertEquals("jquery.js", PathUtil.getJsLibraryName("http://example.com/jquery.js/"));
        assertEquals("jquery.js", PathUtil.getJsLibraryName("http://example.com/jquery.js"));
        assertEquals("jquery.js", PathUtil.getJsLibraryName("http://example.com/jquery.js/?lol=1"));
        assertEquals("jquery.js", PathUtil.getJsLibraryName("http://example.com/JQuErY.js?lol=1"));
        assertEquals("jquery.js", PathUtil.getJsLibraryName("example.com/jquery.js"));
        assertEquals("jquery.js", PathUtil.getJsLibraryName("www.example.com/JQuery.js"));
        assertEquals("jquery.js", PathUtil.getJsLibraryName("/dir/../jquery.js"));
        assertEquals("jquery.js", PathUtil.getJsLibraryName("/jquery.min.js"));
        assertEquals("jquery.js", PathUtil.getJsLibraryName("/jquery.slim.js"));
    }

    /**
     * Testing that the CSS selector defined in pom.xml still works with Google
     */
    public void testGoogleResultsSelector() throws IOException {
        assertTrue(!new GoogleScraper().getLinks("Scalable Capital").isEmpty());
    }

    /**
     * Read HTML file from the resources folder and check scripts using
     * ScriptScraper class
     */
    public void testScriptScraping() {
        // No time left to implement
    }

    /**
     * Mocks GoogleScraper and ScriptScraper using Mockito to call
     * App::getTopLibraries method and check that it correctly groups and sorts the
     * libraries
     */
    public void testTopLibrariesLogic() {
        // No time left to implement
    }
}
