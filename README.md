# library-web-crawler

![Scalable Capital logo](https://uk.scalable.capital/images/3x3i7a9xgm11/35WETwl5Y4MSwQiQCoiOKG/84164eb68921a4d0bc488a0b4a16fa59/Logo_with_picturemark_cmyk.png)

The aim of this test assignment is to write a program in Java which counts top javascript libraries used
in web pages found on Google.

## Notes

The only 3rd party library i used was [JSoup](https://jsoup.org/) because i think [using Regex for HTLML parsing](https://blog.codinghorror.com/parsing-html-the-cthulhu-way/) is bad, it can return false positives unless you create a complex regex and complex validations and it is said "don’t need to create elaborate
or complex parsing algorithms". This gave me time to implement the bonus steps since the library is pretty easy to use and i've used it in the past.

## Bonus steps

- Deduplication: I implemented a basic code to get the last segment from paths. I made sure that it works with every type of path (every type of protocol, paths having query strings, minified Js libraries,  etc.). However i didn't have the time left to remove version from paths, so for example "jquery-1.2.js" and "jquery.js" is considered the same. I would do this using regex.

- Concurrency: I used [Parallelism](https://docs.oracle.com/javase/tutorial/collections/streams/parallelism.html) to crawl different pages simultaneously. My first thought was a fixed-size [ExecutorService](https://docs.oracle.com/javase/7/docs/api/java/util/concurrent/ExecutorService.html) to run different Threads with [CompletableFuture](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/CompletableFuture.html) but since the maximum pages to crawl are 10, I found using the [ForkJoinPool](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ForkJoinPool.html) with ParallelStreams is a good choice (ForkJoinPool uses as many threads as processors in your computer (-1) this is enough for this task). With this approach the crawling runs 3x faster.

- Testing : I have 3 unit tests (\src\test\java\capital\scalable\AppTest.java) which are well descriptive so i will not describe them here. Although there is something important to mention: I'm actually doing a Google Search in a unit test! We don't normally do things like this in a unit test, we can just mock it. But i think in this case it is worth it because Google changes the CSS class names often. With this approach you can make sure your tests are very realistic and can change the CSS selector right away if this specific test fails (This is why i have the selector and other important properties defined in the pom.xml)

## How to run

- Run "mvn install exec:java" to compile, run unit tests and start application

### Tools used for this project

- JDK 8
- Visual Studio Code
- Maven 3.6.0
- JSoup HTML parser